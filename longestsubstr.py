class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        substr  = {}
        
        p       = 0#curr
        q       = 0
        
        if len(s) == 1:
            return 1 
        for i,c in enumerate(s):
            if c in substr:
                    q = max(q, i-p)
                    p = max(p, substr[c]+1)
            substr[c] = i
     
        return max(q,len(s)-p)


