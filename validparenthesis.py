class Solution:
    def isValid(self, s):
        vl = []
        c  = {"]":"[", "}":"{", ")":"("}
        for i in s:
            if i in c.values():
                vl.append(i)
            elif i in c.keys():
                if vl == [] or c[i] != vl.pop():
                    return False
            else:
                return False
        return not vl
