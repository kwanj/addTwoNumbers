# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
   #p and q are the head nodes in list 1&2 respectively
        p      =       l1.val
        q      =       l2.val
        carry   =       0
        while p is not None:
        #x will b the value of either the node or sum of nodes
                x       =       (p + q) + carry
                if x>10:
                        carry = 1

                else:
                    carry = 0 
                return x
		break
#if a node is null, we set it to zero n proceed
        if (p is None or q is None): 
                self.val = 0 
        else: 
                q.next = q 
                p.next = p 
			
