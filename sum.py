"""
this is an algorithm to compute the sum of two nums in a list wen given a particular target.
example list =  [1,2,3] target = 5.this formula will give the index of the  numbers in the list to add to get the target.
"""

class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        list = {}
        for k in range (len(nums)):
            t = target - nums[k]
            if t in list:
                return [list[t], k]
            else:
                 list [nums[k]] = k

