
# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
   	#current node = cn
	dummyHead	= ListNode(0)
	cn		= dummyHead
	p		= l1
	q		= l2
	carry		= 0

	while p is not None or q is not None:
		if p is None:
			x = 0
		else:
			x = p.val
		if q is None:
			y = 0
		else:
			y = q.val
		sum = carry + x + y

		carry = sum / 10

		cn.next = ListNode(sum % 10)
		cn = cn.next
		if p is not None:
			p = p.next
		if q is not None:
			q = q.next

	if carry > 0:
		cn.next = ListNode(carry)

	return dummyHead.next
