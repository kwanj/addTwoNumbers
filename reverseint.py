class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        
        a   = str(x)
    
        
        #or (x < -2,147,483,647):
        
        if a[0] == '-':
            s   = list(a)
            s.pop(0)
            s.append('-')
            s.reverse()
            rev = "".join(s[:])
            if int(rev) > -2147483647:
                return int(rev)
            else:
                return 0
        else :
            rev = rev = a[:: -1]
            if int(rev) <  2147483647:
                return int(rev)
            else:
                return 0
           
        return int(rev)

