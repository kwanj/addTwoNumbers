class Solution(object):
       def longestCommonPrefix(self, strs):
            """
            :type strs: List[str]
            :rtype: str
            """
            if not strs: return ''
            str1 = min(strs)
            str2 = max(strs)
            
 
            
            for i, c in enumerate (str1):
                if c != str2[i]:
                    if len(str1) and len(str2) == 1: return ''
                    return str1[:i]
                    i += 1
            return str1
