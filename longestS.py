class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        substr  = []
        
        p       = 0
        q       = 1
        r       = s[p:q]

        if len(s) == 1:
            return 1
        for p in range(p, len(s)-1) :
            for q in range (q, len(s)):
                if s[q] not in r:
                    if q == len(s)-1:
                        if len(substr) < len(s[p:q + 1]):
                            substr = s[p:q + 1]
                        return len(substr)
                    p += 1
                    
                else:
                    if len(r) > len(substr):
                        substr = r
                    q += 1
        return len(substr)  

